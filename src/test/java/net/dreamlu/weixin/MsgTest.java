package net.dreamlu.weixin;

import com.jfinal.kit.HttpKit;

public class MsgTest {

	public static void main(String[] args) {
		String xml = "<xml>"
				+ "<ToUserName><![CDATA[toUser]]></ToUserName>"
				+ "<FromUserName><![CDATA[fromUser]]></FromUserName>"
				+ "<CreateTime>1348831860</CreateTime>"
				+ "<MsgType><![CDATA[text]]></MsgType>"
				+ "<Content><![CDATA[this is a test]]></Content>"
				+ "<MsgId>1234567890123456</MsgId>"
				+ "</xml>";

		String url = "http://localhost:8080/weixin/msg";
		String outXml = HttpKit.post(url, xml);
		System.out.println(outXml);
	}
}
